package net.manager.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.manager.dao.ManagerDao;
import net.manager.model.Manager;


@WebServlet("/register")
public class ManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ManagerDao managerDao = new ManagerDao();

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManagerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String address = request.getParameter("address");
		String contact = request.getParameter("contact");
		
		Manager manager =new Manager();
		manager.setFirstName(firstName);
		manager.setLastName(lastName);
		manager.setUserName(userName);
		manager.setPassword(password);
		manager.setAddress(address);
		manager.setContact(contact);
		System.out.println("push to DB connection");
		try {
	
			managerDao.registerManager(manager);
			
		}
		catch (Exception e) {
            // TODO Auto-generated catch block
			System.out.println(e);
            e.printStackTrace();
        }

		
		
        response.sendRedirect("managerdetails.jsp");
		
		

	}
}
